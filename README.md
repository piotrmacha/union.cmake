# union.cmake
CMake wrapper for [Union API](https://gitlab.com/union-framework/union-api/) and [Gothic API](https://gitlab.com/union-framework/gothic-api/) exposing them as targets for easy integration in CMake based build systems.

## Usage

Use Git Submodules to fetch the `union.cmake` in your project:
```sh
git submodule add git@gitlab.com:piotrmacha/union.cmake.git
git submodule update --remote --recursive   # To fetch the newest version 
```

Then you can include the `union.cmake` directory in root CMakeLists.txt:
```cmake
# [...]
add_sudirectory(union.cmake)
# [...]
```

### Build configuration

CMake exposes following options to configure the build:
#### UNION_API_DEBUG
Build Union in debug mode (default OFF)
#### UNION_API_LIBRARY_TYPE
Linking type of library (default: SHARED, available: SHARED, STATIC)
#### GOTHIC_API
Configure target for gothic-api (default: ON)

### Targets

#### union-api
Union API build as static or dynamic library depending on `UNION_API_LIBRARY_TYPE`.  

#### gothic-api
Gothic API exposed as INTERFACE library (header only).

### Linking project
To link Union API to your project, you only need to specify `union-api` target in `target_link_libraries`:
```cmake
cmake_minimum_required(VERSION 3.25)
project(union_example_cmake)
set(CMAKE_CXX_STANDARD 20)

option(GOTHIC_API "" OFF)
add_subdirectory(union.cmake)

add_library(union_example_cmake SHARED)
target_sources(union_example_cmake PRIVATE "Plugin.hpp")
target_link_libraries(union_example_cmake PRIVATE union-api)
```

To use standard Gothic API, just add `gothic-api` dependency to the library:
```cmake
cmake_minimum_required(VERSION 3.25)
project(union_example_cmake)
set(CMAKE_CXX_STANDARD 20)

option(GOTHIC_API "" OFF)
add_subdirectory(union.cmake)

add_library(union_example_cmake SHARED)
target_sources(union_example_cmake PRIVATE "Plugin.hpp")
target_link_libraries(union_example_cmake PRIVATE union-api gothic-api)
```

In case you'd like to create custom class methods using UserAPI, the easiest way is to just copy [gothic-api](https://gitlab.com/union-framework/gothic-api/) to your project and configure the build directly inside your library:
```cmake
cmake_minimum_required(VERSION 3.25)
cmake_minimum_required(VERSION 3.25)
project(union_example_cmake)
set(CMAKE_CXX_STANDARD 20)

option(GOTHIC_API "" OFF)
add_subdirectory(union.cmake)

add_library(union_example_cmake SHARED)
target_sources(union_example_cmake PRIVATE "Plugin.hpp" "gothic-api/ZenGin/zGothicAPI.cpp")
target_compile_definitions(union_example_cmake PRIVATE __G1 __G1A __G2 __G2A)
target_include_directories(union_example_cmake PRIVATE "gothic-api")
target_link_directories(union_example_cmake PRIVATE "gothic-api")
target_link_libraries(union_example_cmake PRIVATE union-api)
```

## Example
Example project is available at [gitlab.com/piotrmacha/union_example_cmake](https://gitlab.com/piotrmacha/union_example_cmake)